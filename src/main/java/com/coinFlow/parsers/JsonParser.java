package com.coinFlow.parsers;

import com.coinFlow.data.CoinsData;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class JsonParser {

    public CoinsData parsingJsonFile() throws IOException {
        Gson gson = new Gson();

        JsonReader reader = new JsonReader(new FileReader("src/main/resources/testJson/jsonTestRates.json"));

        CoinsData parsedData = gson.fromJson(reader, CoinsData.class);

        reader.close();
        return parsedData;
    }

}
