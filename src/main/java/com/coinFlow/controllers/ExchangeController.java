package com.coinFlow.controllers;

import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.bittrex.BittrexExchange;
import org.knowm.xchange.bittrex.dto.marketdata.BittrexChartData;
import org.knowm.xchange.bittrex.dto.marketdata.BittrexMarketSummary;
import org.knowm.xchange.bittrex.dto.marketdata.BittrexTicker;
import org.knowm.xchange.bittrex.service.BittrexChartDataPeriodType;
import org.knowm.xchange.bittrex.service.BittrexMarketDataServiceRaw;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.OrderBook;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.dto.trade.LimitOrder;
import org.knowm.xchange.service.marketdata.MarketDataService;
import org.knowm.xchange.service.marketdata.params.Params;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@RestController
public class ExchangeController {

    public ExchangeController() {
    }

    MarketDataService marketDataService;

    @RequestMapping("/exchange")
    public BittrexTicker getExchangeRates() throws IOException {
        ExchangeController exchangeController = new ExchangeController();
        Ticker ticker;

        Exchange bittrex = ExchangeFactory.INSTANCE.createExchange(BittrexExchange.class.getName());
        marketDataService = bittrex.getMarketDataService();
//        MarketDataService marketDataService2 = bittrex.getMarketDataService();



        System.out.println(" ");


        System.out.println(" ");
        System.out.println(exchangeController.generic(marketDataService));
        System.out.println(exchangeController.raw((BittrexMarketDataServiceRaw) marketDataService));
        return exchangeController.raw((BittrexMarketDataServiceRaw) marketDataService);
    }

    @RequestMapping("/orders")
    public OrderBook getOrders() throws IOException {
        Exchange bittrex = ExchangeFactory.INSTANCE.createExchange(BittrexExchange.class.getName());
        marketDataService = bittrex.getMarketDataService();

        OrderBook orderBook = marketDataService.getOrderBook(CurrencyPair.BTC_USD);

        System.out.println(orderBook.getTimeStamp());

        return marketDataService.getOrderBook(CurrencyPair.BTC_USD);

    }

    @RequestMapping("/date")
    public List<LimitOrder> getCurrentDate() throws IOException {


        Exchange bittrex = ExchangeFactory.INSTANCE.createExchange(BittrexExchange.class.getName());
        marketDataService = bittrex.getMarketDataService();
        OrderBook orderBook = marketDataService.getOrderBook(CurrencyPair.BTC_USD);

        List<LimitOrder> AskLimitOrderList = orderBook.getAsks();

        for (LimitOrder limitOrder : AskLimitOrderList) {
            System.out.println(limitOrder.getLimitPrice());
        }

        return AskLimitOrderList;
    }


    @RequestMapping("/bittrex")
    public ArrayList<BittrexChartData> getBittrexData() throws IOException {
//        BittrexChartData bittrexChartData = (BittrexChartData) marketDataService;
        Exchange bittrex = ExchangeFactory.INSTANCE.createExchange(BittrexExchange.class.getName());
        marketDataService = bittrex.getMarketDataService();

        BittrexMarketDataServiceRaw bittrexMarketDataServiceRaw = (BittrexMarketDataServiceRaw) marketDataService;

//        return bittrexMarketDataServiceRaw.getBittrexMarketSummary("BTC_USD");

        return bittrexMarketDataServiceRaw.getBittrexChartData(CurrencyPair.BTC_USD, BittrexChartDataPeriodType.ONE_HOUR);

    }




    private Ticker generic(MarketDataService marketDataService) throws IOException {
        return marketDataService.getTicker(CurrencyPair.BTC_USD);
    }



    private BittrexTicker raw(BittrexMarketDataServiceRaw marketDataService) throws IOException {
        return marketDataService.getBittrexTicker(CurrencyPair.BTC_USD);
    }
}
