package com.coinFlow.controllers;

import com.coinFlow.data.CoinsData;
import com.coinFlow.parsers.JsonParser;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.io.IOException;


@RestController
public class OutputController {

    @RequestMapping("/coinflow")
    private CoinsData output() throws IOException {
        return new JsonParser().parsingJsonFile();
    }
}

//"<table>" + "<tr>" +  "<td>" + "Text1"+ "</td>" + "<td>" + "Text2"+ "</td>" + "<tr>" + "</table>"