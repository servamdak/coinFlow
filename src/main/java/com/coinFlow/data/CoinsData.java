package com.coinFlow.data;

public class CoinsData {


    private String price;
    private String data;
    private String change;
    private String minMax;
    private String volatility;
    private int volume;



    public CoinsData(String price, String data, String change, String minMax, String volatility, int volume) {
        this.price = price;
        this.data = data;
        this.change = change;
        this.minMax = minMax;
        this.volatility = volatility;
        this.volume = volume;
    }

    public CoinsData() {
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public void setMinMax(String minMax) {
        this.minMax = minMax;
    }

    public void setVolatility(String volatility) {
        this.volatility = volatility;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public String getPrice() {
        return price;
    }

    public String getData() {
        return data;
    }

    public String getChange() {
        return change;
    }

    public String getMinMax() {
        return minMax;
    }

    public String getVolatility() {
        return volatility;
    }

    public int getVolume() {
        return volume;
    }
}
